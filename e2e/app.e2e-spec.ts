import { SmsfrontendPage } from './app.po';

describe('smsfrontend App', function() {
  let page: SmsfrontendPage;

  beforeEach(() => {
    page = new SmsfrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
