import { Component } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
  moduleId:module.id,
  selector: 'app-root',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent {
}

