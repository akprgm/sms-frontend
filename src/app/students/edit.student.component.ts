import { Component, Injectable} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { Http, Response,Headers, RequestOptions} from '@angular/http';
import {Router} from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../environments/environment';
import {StudentModel}  from './student.model';
import {Logout} from '../users/logout';

@Component({
  moduleId: module.id,
  selector: '',
  templateUrl: '../../assets/templates/editStudent.html',
  styleUrls: []
})

@Injectable()
export class EditStudentComponent extends Logout{
  public studentId;
  public Model;
  public interest;
  constructor( private _http: Http, private route: ActivatedRoute, private router:Router){
    super();
    this.interest = new Array();
    this.Model = new StudentModel("ak",2018,"sushant lok 1","male",new Array());

  }

  ngOnInit() {
    let id;
    this.route.params.subscribe((data) => id=data['id']); 
    this.getStudent(id);
  }
  
  public logOut ()
  {
    super.logOut();
    this.router.navigate(['../../login']);
    return;
  }

  public addInterest ()
  {
      this.interest.push({});    
  }

  public removeInterest (i)
  {
    console.log(i);
    if (i<this.interest.length) {
       this.interest.splice(i,1)      
    } 
  }

  public getStudent (studentId)
  {
    this.studentId = studentId;
    let geturl = environment.apiUrl+'/students'+'/'+studentId;
    if(window.sessionStorage.getItem('token')){
      let token = 'bearer:'+window.sessionStorage.getItem('token');
      let headers = new Headers({'Authorization': token});
      let response = this._http.get(geturl,{headers: headers})
      .map(res => res.json())
      .subscribe(
        (data) => {
          for ( let i=0; i<data.data.interest.length; i++) {
            this.interest.push({"value":data.data.interest[i]});
          }
          this.Model= new StudentModel(data.data.name, data.data.year_of_passing, data.data.address,data.data.gender,data.data.interest);
          console.log(this.interest);
          return; 
        },
        (err) => {return;});
    }else{
          this.router.navigate(['../../login']);
    }  
  }

  public editStudent ()
  {
    let updateUrl = environment.apiUrl+'/students'+'/'+this.studentId;

    let body ={
      "name": this.Model.name,
      "year_of_passing": this.Model.year_of_passing,
      "gender": this.Model.gender,
      "interest": new Array(),
      "address": this.Model.address
    }

    for(let val in this.interest) {
      body.interest.push(this.interest[val].value);
    }
    
    let token = 'bearer:'+window.sessionStorage.getItem('token');    
    let headers = new Headers({'Authorization': token});
    headers.append('Content-Type','application/json');
    let response = this._http.patch(updateUrl,body,{headers: headers})
    .map(res => res.json())
    .subscribe((data) => {
        this.Model.message = data.data;
        this.router.navigate(['/students']);      
      },
      (err) => {
        this.Model.message = "Invalid input credentials";
    });
  }
}