import { Component, Injectable} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { Http, Response,Headers, RequestOptions} from '@angular/http';
import {Router} from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';
import {StudentModel}  from './student.model';
import { environment } from '../../environments/environment';
import {Logout} from '../users/logout';

@Component({
  selector: '',
  templateUrl: '../../assets/templates/addStudent.html',
  styleUrls: []
})

@Injectable()
export class AddStudentComponent extends Logout
{
  private addurl =  environment.apiUrl+`/students`;
  public Model;
  public interest;
  public formEnable = true;

  constructor( private _http: Http, private router:Router)
  {
    super();
    this.interest = [];
    this.Model = new StudentModel("", 2016, "", "male", new Array());
  }

   public addInterest ()
  {
      this.interest.push({});    
  }

  public logOut ()
  {
    super.logOut();
    this.router.navigate(['../login']);
    return;
  }

  public removeInterest (i)
  {
    if (i<this.interest.length) {
       this.interest.splice(i,1)      
    } 
  }

  public addStudent ()
  {
    this.formEnable = false;
    for(let val in this.interest) {
      this.Model.interest.push(this.interest[val].value);
    }

    let body ={
      "name": this.Model.name,
      "year_of_passing": this.Model.year_of_passing,
      "gender": this.Model.gender,
      "interest": this.Model.interest,
      "address": this.Model.address
    }
    let token = 'bearer:'+window.sessionStorage.getItem('token');   

    let headers = new Headers({'Authorization': token});
    
    headers.append('Content-Type','application/json');
    
    let response = this._http.post(this.addurl,body,{headers: headers})
    .map(res => res.json())
    .subscribe((data) => {
        this.Model.message = data.data;
        this.router.navigate(['/students']);
        this.formEnable = true;
    },
      (err) => {
        this.Model.message = JSON.parse(err._body).data;
        this.formEnable = true;
    });
    
  }
}