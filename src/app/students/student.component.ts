import { Component, Injectable, ChangeDetectorRef} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { Http, Response,Headers, RequestOptions} from '@angular/http';
import {Router} from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';
import {Logout} from '../users/logout';
import { environment } from '../../environments/environment';

@Component({
  moduleId: module.id,
  selector: '',
  templateUrl: '../../assets/templates/student.html',
  styleUrls: ['../../assets/css/app.css']
})

@Injectable()
export class StudentComponent extends Logout{  
  public Active;
  public data;
  public students;
  public error;

  constructor( private _http: Http, private detectChange: ChangeDetectorRef, private router: Router){
    super();
    this.Active = false;
   }
  
  ngOnInit() 
  { 
    this.getstudents();
  }

  public logOut ()
  {
    super.logOut();
    this.router.navigate(['/login']);
    return;
  }

  public getstudents (id:any = 0)
  {
    let geturl = environment.apiUrl+`/students`;
  
    if(id ==='next' && (this.data.current_page < this.data.total)){
      geturl = this.data.next_page_url;
    }else if( id ==='prev' && this.data.current_page-1){
      geturl = this.data.prev_page_url;
    }
    
    if (window.sessionStorage.getItem('token')) {
    
      let token = 'bearer:'+window.sessionStorage.getItem('token');
    
      this.Active = true;
    
      let headers = new Headers({'Authorization': token});
    
      let response = this._http.get(geturl,{headers: headers})
    
      .map(res => {
    
        if(res.status ===401){
    
          window.sessionStorage.clear();
    
          this.router.navigate(['/login']);
    
      }
  
        return res.json()
  
      })
      .subscribe(
        
        (data) => {
          if(data){
            this.students = (data)?data.data.data:"";
            this.data = data.data;
            this.detectChange.reattach();  
          }
          return;
        },
        
        (err) => { this.error = err; return;});
    
    }else{
          this.router.navigate(['/login']);
    }  
  }


  public delete (studentId)
  {
    var confirm = window.confirm("Are you sure");
    if(!confirm){
      return;
    }
    let deleteurl = environment.apiUrl+'/students'+'/'+studentId;
    
    let token = 'bearer:'+window.sessionStorage.getItem('token');
    
    this.Active = true;
    
    let headers = new Headers({'Authorization': token});
    
    let response = this._http.delete(deleteurl,{headers: headers})
    
    .map(res => res.json())
    
    .subscribe(
    
      (data) => { this.router.navigate(['/students']);this.getstudents();},
    
      (err) => { this.error = err;});
    
    return;
    
  }
}