import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule }   from '@angular/router';
import { AlertModule } from 'ng2-bootstrap';

import { AppComponent } from './app.component';
import { LoginComponent } from './users/login.component';
import { RegisterComponent } from './users/register.component';
import { StudentComponent } from './students/student.component';
import { AddStudentComponent } from './students/add.student.component';
import { EditStudentComponent } from './students/edit.student.component';
import { StudentShellComponent } from './students/shell.student.component';
import { ForgetPasswordComponent } from './users/forgetPassword.component';
import { ChangePasswordComponent } from './users/changePassword.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    StudentComponent,
    AddStudentComponent,
    EditStudentComponent,
    StudentShellComponent,
    ForgetPasswordComponent,
    ChangePasswordComponent
  ],
  imports: [
    AlertModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      {
        path: '',
        redirectTo: 'login',
        pathMatch : 'full'
      },
      {
        path:'login',
        component: LoginComponent
      },
      {
        path: 'register',
        component: RegisterComponent
      },
      {
        path: 'forgetPassword',
        component: ForgetPasswordComponent
      },
      {
        path: 'changePassword',
        component: ChangePasswordComponent
      },
      { 
        path: 'students',
        component: StudentComponent
      },
      {
        path: 'students',
        component: StudentShellComponent,
        children:[
          {
            path: "add",
            component: AddStudentComponent
          },
          {
            path: 'edit/:id',
            component: EditStudentComponent
          },
        ]
      }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
