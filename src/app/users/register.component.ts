import { Component, OnInit, Injectable} from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Http, Response,Headers, RequestOptions} from '@angular/http';
import {Router} from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';
import { UserRegister } from './user.model';
import { environment } from '../../environments/environment';

@Component({
  moduleId:module.id,
  selector: 'register-root',
  templateUrl: '../../assets/templates/signup.html',
  styleUrls: []
})

@Injectable()
export class RegisterComponent
{
  public Model;
  public response;
  public registerUrl;
  public loader=false;
  constructor( private _http: Http, private router:Router){
    this.Model =  new UserRegister("","","","","");
    this.registerUrl = environment.apiUrl+"/users/register";
  }

  ngOnInit() 
  { 
    this.checkLogin();
  }

  public checkLogin ()
  {
    if (window.sessionStorage.getItem('token')) {
        this.router.navigate(['/students']);
    }
  }

  public register ()
  {
    if (!(this.Model.password===this.Model.confirmPassword)) {
      this.Model.message = "password doesn't match";
      return;
    } 
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    let body = {
      "name": this.Model.name,
      "email": this.Model.email,
      "password": this.Model.password
    }
    this.loader = true;
    let response = this._http.post(this.registerUrl,body,{headers:headers})
    .map(res => res.json())
    .subscribe(
      (data) => {
        this.Model.message =  data.data;
        this.loader = false; 
      },
      (err) => {
        this.Model.message = JSON.parse(err._body).data;
        this.loader = false;
      });
  }
}

