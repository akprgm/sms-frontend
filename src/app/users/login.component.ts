import { Component, Injectable } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Http, Response,Headers, RequestOptions} from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';
import {User} from './user.model';
import { environment } from '../../environments/environment';


@Component({
  moduleId:module.id,
  selector: '',
  templateUrl: '../../assets/templates/signin.html',
  styleUrls: []
})

@Injectable()
export class LoginComponent
{
  public Model;
  public response;
  public loginUrl;
  constructor( private _http: Http, private router: Router){
    this.Model =  new User("","","");
    this.loginUrl = environment.apiUrl+"/users/login";
  }

  ngOnInit() 
  { 
    this.checkLogin();
  }

  public checkLogin ()
  {
    if (window.sessionStorage.getItem('token')) {
      this.router.navigate(['/students']);      
    }
  }

  public login ()
  {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    let body = {
      "email": this.Model.email,
      "password": this.Model.password
    }
    let response = this._http.post(this.loginUrl,body,{headers:headers})
    .map(res => res.json())
    .subscribe(
      (data) => {
        let storage = window.sessionStorage;
        storage.setItem("token",data.data);
        this.router.navigate(['/students']);
      },
      (err) => {
        this.Model.message = JSON.parse(err._body).data;
      });
  }

}

