import { Component, Injectable} from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Http, Response,Headers, RequestOptions} from '@angular/http';
import {Router, ActivatedRoute} from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../environments/environment';

@Component({
  moduleId:module.id,
  selector: '',
  templateUrl: '../../assets/templates/changePassword.html',
  styleUrls: []
})

@Injectable()
export class ChangePasswordComponent
{
  public Model;
  public token ;
  public response;
  public changeUrl;
  constructor( private _http: Http, private activatedRoute: ActivatedRoute, private router:Router){
    this.activatedRoute.queryParams.subscribe((params) => {
        this.token = params['token'];
    });
    this.Model =  {"password":"", "message":""};
    this.changeUrl = environment.apiUrl+"/users/changePassword?token="+this.token;
  }

  ngOnInit() 
  { 
    this.checkLogin();
  }

  public checkLogin ()
  {
    if (window.sessionStorage.getItem('token')) {
        this.router.navigate(['/login']);
        this.router.navigate(['/students']);
    }
  }

  public changePassword ()
  {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    let response = this._http.post(this.changeUrl,{"password":this.Model.password},{headers:headers})
    .map(res => res.json())
    .subscribe(
      (data) => {
        this.Model.message = data.data;
        this.router.navigate(['/login']);       
      },
      (err) => {
        this.Model.message = "Url expired, go to forget Password";
      });
  }

}

