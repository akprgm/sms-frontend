import { Component, Injectable } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Http, Response,Headers, RequestOptions} from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';
import {User} from './user.model';
import { environment } from '../../environments/environment';

@Component({
  moduleId:module.id,
  selector: '',
  templateUrl: '../../assets/templates/forgetPassword.html',
  styleUrls: []
})

@Injectable()
export class ForgetPasswordComponent
{
  public Model;
  public response;
  public forgetUrl;
  public loader=false;
  constructor( private _http: Http, private router: Router ){
    this.Model =  new User("","akak","");
    this.forgetUrl = environment.apiUrl+"/users/forgetPassword?email=";
  }

  ngOnInit() 
  { 
    this.checkLogin();
  }

  public checkLogin ()
  {
    if (window.sessionStorage.getItem('token')) {
        this.router.navigate(['/students']);
    }
  }

  public forgetPassword ()
  {
    this.loader=true;
    let headers = new Headers();
    this.forgetUrl += this.Model.email;
    headers.append('Content-Type','application/json');
    let response = this._http.get(this.forgetUrl,{headers:headers})
    .map(res => res.json())
    .subscribe(
      (data) => {
        this.loader=false;       
        this.Model.message = data.data; 
      },
      (err) => {
        this.loader=false
        this.Model.message = JSON.parse(err._body).data;
      });
  }

}

